FROM golang:1.16-alpine
WORKDIR /app
COPY *.go /app
RUN go mod init example.com/m && go build -o service main.go
EXPOSE 11130
CMD ["./service"]
