package main

import (
	"io"
	"net/http"
)

func helloHandler(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Hello janbo from @MRStark703")
}

func main() {
	http.HandleFunc("/", helloHandler)
	http.ListenAndServe(":11130", nil)
}
