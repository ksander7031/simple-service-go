# simple-service-go



## Simple web app
simple web application written in go
built from docker file and uploaded to docker hub
deploys to k8s cluster located in GCE
the response from the application can be obtained from the link(http://janbo.ksanderk8s.org/)

nginx ingress controller solution was chosen as the most popular 
and recommended solution for traffic redirection.
the cluster has cert-manager configured to automatically obtain letsencrypt certificates.

### Troubles

letsencrypt did not issue a certificate. 
In the process of debugging and studying the acme protocol, it turned out that letsencrypt cannot validate the domain because it does not get access to the cert-manager pods

###Solution
installing nginx-ingress previous version.
setting in namespace according to GKE documentation.